package br.com.willy.jokenpo.objects;

import java.util.List;

import br.com.willy.jokenpo.enums.JokenpoHandsEnum;
import br.com.willy.jokenpo.utils.Utils;

public class NormalPlayer extends Player {

	private String name;
	private List<Integer> handsPlays;
	
	public NormalPlayer(String name) {
		this.name = name;
	}
	
	@Override
	public JokenpoHandsEnum getHand(Integer round) {
		JokenpoHandsEnum handEnum = null;
		Integer handValue = this.handsPlays.get(round);
		if (handValue == JokenpoHandsEnum.PAPER_HAND.getHand())
			handEnum = JokenpoHandsEnum.PAPER_HAND;
		if (handValue == JokenpoHandsEnum.ROCK_HAND.getHand())
			handEnum = JokenpoHandsEnum.ROCK_HAND;
		if (handValue == JokenpoHandsEnum.SCISSORS_HAND.getHand())
			handEnum = JokenpoHandsEnum.SCISSORS_HAND;
		return handEnum;
	}
	
	@Override
	public void genarateHandsPlays(int nrounds) {
		this.handsPlays = Utils.generateRandonNumbers(nrounds, 3);
	}

	@Override
	public List<Integer> getHandsPlays() {
		return this.handsPlays;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setNome(String name) {
		this.name = name;
	}
	
}

package br.com.willy.jokenpo.enums;

public enum JokenpoHandsEnum {
	
	ROCK_HAND(1), 
	PAPER_HAND(2), 
	SCISSORS_HAND(3);
	
	private final Integer hand;
	
	JokenpoHandsEnum(Integer hand) {
		this.hand = hand;
	}
	
	public Integer getHand() {
		return this.hand;
	}

}

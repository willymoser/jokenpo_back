package br.com.willy.jokenpo.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.willy.jokenpo.DTO.ResultMatchDTO;
import br.com.willy.jokenpo.enums.JokenpoHandsEnum;
import br.com.willy.jokenpo.model.MatchModel;
import br.com.willy.jokenpo.model.RoundModel;
import br.com.willy.jokenpo.objects.NormalPlayer;
import br.com.willy.jokenpo.objects.PaperPlayer;
import br.com.willy.jokenpo.objects.Player;
import br.com.willy.jokenpo.repository.MatchRepository;
import br.com.willy.jokenpo.repository.RoundRepository;

@Service
public class RoundService {
	
	@Autowired
	private RoundRepository roundRepository;
	
	@Autowired
	private MatchRepository matchRepository;
	
	public List<ResultMatchDTO> getResultByMatchId(long matchId) {
		long numberOfRounds = roundRepository.countRoundsByMatchId(matchId);
		List<ResultMatchDTO> results = new ArrayList<ResultMatchDTO>();
		List<Object> players = roundRepository.findPlayersByMatchId(matchId);
		
		Object row = players.get(0);
		Object[] o = (Object[]) row;
		for (int i = 0; i < o.length; i++) {
			long v = roundRepository.countVictoriesByMatchIdAndPlayername(matchId, o[i].toString());
			long d = roundRepository.countDefeatsByMatchIdAndPlayername(matchId, o[i].toString());
			long e = numberOfRounds - v - d;
			results.add(new ResultMatchDTO(o[i].toString(), v, d, e));
		};

		return results; 
	}
	
	public MatchModel runJokenpo(Integer nRounds) {
		
		MatchModel match = new MatchModel();
		match.setUpdatedAt(new Date());
		match.setCreatedAt(new Date());
		List<RoundModel> rounds = new ArrayList<RoundModel>();
		
		Player player1 = new PaperPlayer("PaperPlayer");
		player1.genarateHandsPlays(nRounds);
		Player player2 = new NormalPlayer("NormalPlayer");
		player2.genarateHandsPlays(nRounds);
		
		for (int roundNumber = 0; roundNumber < nRounds; roundNumber++) {
			rounds.add(getResultRound(player1, player2, roundNumber));
		}
		
		matchRepository.save(match);
		for (RoundModel r : rounds) {
			r.setMatch(match);
		}
		roundRepository.saveAll(rounds);
		
		return match;
		
	}
	
	public ResultMatchDTO ResultByMatchId(Integer matchId) {
		
		return null;
	}
	
	private RoundModel getResultRound(Player p1, Player p2, Integer roundNumber) {
		
		RoundModel round = new RoundModel();
		round.setCreatedAt(new Date());
		round.setUpdatedAt(new Date());
		
		if (p1.getHand(roundNumber) == p2.getHand(roundNumber)) {
			round.setWinner("Empate");
			round.setLoser("Empate");
		} else {
			if (JokenpoHandsEnum.PAPER_HAND.equals(p1.getHand(roundNumber)) &&
					!JokenpoHandsEnum.SCISSORS_HAND.equals(p2.getHand(roundNumber))) {
				round.setWinner(p1.getName());
				round.setLoser(p2.getName());
			} else {
				round.setWinner(p2.getName());
				round.setLoser(p1.getName());
			}	
		}
		
		return round;
		
	}

}

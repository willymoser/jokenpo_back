package br.com.willy.jokenpo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.willy.jokenpo.DTO.MatchDTO;
import br.com.willy.jokenpo.DTO.ResultMatchDTO;
import br.com.willy.jokenpo.model.MatchModel;
import br.com.willy.jokenpo.service.RoundService;

@RestController
@RequestMapping(value="/jokenpo")
public class PlayGameController {
	
	@Autowired
	private RoundService roundService;
	
	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping("/playgame")
	public ResponseEntity<List<ResultMatchDTO>> playGame(@Valid @RequestBody MatchDTO matchdto) {
		
		MatchModel matchModel = this.roundService.runJokenpo(matchdto.getNumberRounds());
		System.out.println("Match ID" + matchModel.getId());
		List<ResultMatchDTO> result = roundService.getResultByMatchId(matchModel.getId());
		return ResponseEntity.ok().body(result);
		
	}

}

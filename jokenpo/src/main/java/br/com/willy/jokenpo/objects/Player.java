package br.com.willy.jokenpo.objects;

import java.util.List;

import br.com.willy.jokenpo.enums.JokenpoHandsEnum;

public abstract class Player {

	public abstract String getName();
	public abstract void genarateHandsPlays(int nrounds);
	public abstract List<Integer> getHandsPlays();
	public abstract JokenpoHandsEnum getHand(Integer round);
	
}

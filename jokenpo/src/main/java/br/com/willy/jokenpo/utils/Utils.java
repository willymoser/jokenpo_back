package br.com.willy.jokenpo.utils;

import java.util.ArrayList;
import java.util.Random;
import java.util.List;

public class Utils {
	
	public static List<Integer> generateRandonNumbers(long length, Integer range) 
	{
		Random gerador = new Random();
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < length; i++) {
			list.add(gerador.nextInt(range) + 1);
		}
		return list;
	}
	
}

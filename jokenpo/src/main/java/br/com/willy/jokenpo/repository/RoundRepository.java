package br.com.willy.jokenpo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.willy.jokenpo.model.RoundModel;

@Repository
public interface RoundRepository extends JpaRepository<RoundModel, Integer> {
	
	@Query(value = "SELECT u FROM RoundModel u WHERE u.match.id = :matchid")
	List<RoundModel> findRoundsByMatchId(@Param("matchid") long matchid);
	
	@Query(value = "SELECT winner, loser FROM rounds WHERE match_id = :matchid and winner != 'Empate' LIMIT 1", nativeQuery = true)
	List<Object> findPlayersByMatchId(@Param("matchid") long matchid);
	
	@Query(value = "SELECT count(*) FROM rounds WHERE match_id = :matchid and winner = :playername LIMIT 1", nativeQuery = true)
	Integer countVictoriesByMatchIdAndPlayername(@Param("matchid") long matchid, @Param("playername") String playername);
	
	@Query(value = "SELECT count(*) FROM rounds WHERE match_id = :matchid and loser = :playername LIMIT 1", nativeQuery = true)
	Integer countDefeatsByMatchIdAndPlayername(@Param("matchid") long matchid, @Param("playername") String playername);
	
	@Query(value = "SELECT count(*) FROM rounds WHERE match_id = :matchid", nativeQuery = true)
	Integer countRoundsByMatchId(@Param("matchid") long matchid);
	
}

package br.com.willy.jokenpo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.willy.jokenpo.model.MatchModel;

@Repository
public interface MatchRepository extends JpaRepository<MatchModel, Integer> {

}

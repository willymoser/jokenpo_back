package br.com.willy.jokenpo.DTO;

import java.io.Serializable;

public class ResultMatchDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String playerName;
	private long victories;
	private long defeats;
	private long empates;
	
	public ResultMatchDTO() { }
	
	public ResultMatchDTO(String playerName, long victories, long defeats, long empates) {
		super();
		this.playerName = playerName;
		this.victories = victories;
		this.defeats = defeats;
		this.empates = empates;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public long getVictories() {
		return victories;
	}

	public void setVictories(Integer victories) {
		this.victories = victories;
	}

	public long getDefeats() {
		return defeats;
	}

	public void setDefeats(Integer defeats) {
		this.defeats = defeats;
	}

	public long getEmpates() {
		return this.empates;
	}

	public void setEmpates(Integer numbeOfRounds) {
		this.empates = numbeOfRounds;
	}

}

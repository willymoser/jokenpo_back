package br.com.willy.jokenpo.DTO;

import java.io.Serializable;

public class MatchDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer numberRounds;
	
	public MatchDTO() { }

	public Integer getNumberRounds() {
		return numberRounds;
	}

	public void setNumberRounds(Integer numberRounds) {
		this.numberRounds = numberRounds;
	}
	
}

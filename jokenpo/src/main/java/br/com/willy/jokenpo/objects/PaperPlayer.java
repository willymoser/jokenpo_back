package br.com.willy.jokenpo.objects;

import java.util.ArrayList;
import java.util.List;

import br.com.willy.jokenpo.enums.JokenpoHandsEnum;

public class PaperPlayer extends Player {
	
	private String name;
	private List<Integer> handsPlays;
	
	public PaperPlayer(String name) {
		this.name = name;
	}
	
	@Override
	public JokenpoHandsEnum getHand(Integer round) {
		JokenpoHandsEnum handEnum = null;
		Integer handValue = this.handsPlays.get(round);
		if (handValue == JokenpoHandsEnum.PAPER_HAND.getHand())
			handEnum = JokenpoHandsEnum.PAPER_HAND;
		if (handValue == JokenpoHandsEnum.ROCK_HAND.getHand())
			handEnum = JokenpoHandsEnum.ROCK_HAND;
		if (handValue == JokenpoHandsEnum.SCISSORS_HAND.getHand())
			handEnum = JokenpoHandsEnum.SCISSORS_HAND;
		return handEnum;
	}
	
	@Override
	public void genarateHandsPlays(int nrounds) {
		List<Integer> hands = new ArrayList<>();
		for (int i = 0; i < nrounds; i++) {
			hands.add(JokenpoHandsEnum.PAPER_HAND.getHand());
		}
		this.handsPlays = hands;
	}
	
	@Override
	public List<Integer> getHandsPlays() {
		return this.handsPlays;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setNome(String name) {
		this.name = name;
	}
	
}
